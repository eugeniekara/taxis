import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;

/**
 * La classe de test TaxiTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class TaxiTest extends TestCase
{
    private Taxi taxi;
    private Passager passager;
    
    /**
     * Constructeur par défaut de la classe de test TaxiTest
     */
    public TaxiTest()
    {
    }

    /**
     * Crée un taxi.
     *
     * Appelé avant chaque cas de test.
     */
    @Before
    protected void setUp()
    {
        TaxiCompany company = new TaxiCompany();
        // Position de départ pour le taxi.
        Position taxiPosition = new Position(0, 0);
        // Positions du passager.
        Position prise_en_charge = new Position(1, 2);
        Position destination = new Position(5, 6);
        
        passager = new Passager(prise_en_charge, destination);
        taxi = new Taxi(company, taxiPosition);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    protected void tearDown()
    {
    }
    
    /**
     * Test de création et de l'état initial d'un taxi.
     */
    @Test
    public void testCreation()
    {
        assertEquals(true, taxi.estLibre());
    }
    
    /**
     * Teste qu'un taxi n'est plus libre après qu'il a
     * pris en charge un passager.
     */
    @Test
    public void testPriseEnCharge()
    {
        taxi.prendEnCharge(passager);
        assertEquals(false, taxi.estLibre());
    }
    
    /**
     * Teste qu'un taxi devient libre après avoir fait descendre
     * un passager.
     */
    public void testFaitDescendre()
    {
        taxi.prendEnCharge(passager);
        assertEquals(false, taxi.estLibre());
        taxi.faitDescendrePassager();
        assertEquals(true, taxi.estLibre());
    }
    
    /**
     * Teste qu'un taxi effectue sa course
     * dans un nombre d'étapes raisonnable.
     */
    public void testCourse()
    {
        Position priseEnChargePosition = passager.getPriseEnChargePosition();
        Position destination = passager.getDestination();
        // Le nombre d'étapes attendues est la somme de la distance
        // du taxi au passager et de la distance du passager à la
        // destination.
        int etapesAttendues = taxi.getPosition().distance(priseEnChargePosition) +
                             priseEnChargePosition.distance(destination);
        
        taxi.prendEnCharge(passager);

        int etapes = 0;
        while(!taxi.estLibre() && etapes < etapesAttendues) {
            taxi.act();
            etapes++;
        }
        assertEquals(etapes, etapesAttendues);
        assertEquals(taxi.estLibre(), true);
    }
}

