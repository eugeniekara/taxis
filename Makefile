
JFLAGS = -g -Xlint:unchecked
JC = javac
.SUFFIXES: .java .class
.java.class:
				$(JC) $(JFLAGS) $*.java

CLASSES = \
				Demo.java \
				Actor.java \
				MissingPassengerException.java \
				Passager.java \
				PassagerSource.java \
				Position.java \
				Taxi.java \
				TaxiCompany.java \
				Vehicule.java

default: classes

all: classes docs

docs: README.md entete.md
	pandoc -o README.pdf entete.md README.md

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class *.ctxt

mrproper: clean
	$(RM) README.pdf
